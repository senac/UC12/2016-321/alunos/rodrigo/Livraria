/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livraria.bean;

import br.com.senac.livraria.banco.AutorDAO;
import br.com.senac.livraria.entity.Autor;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Administrador
 */
@Named(value = "autorBean")
@RequestScoped
public class AutorBean {
    
    
    private Autor autor = new Autor(); 
    private AutorDAO dao = new AutorDAO(); 

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    
    public void salvar(){
        
        try{
            if(this.autor.getId() == 0 ){
                dao.save(autor);
            }else{
                dao.update(autor);
            }            
            //Salvo com sucesso
            FacesContext context = FacesContext.getCurrentInstance() ; 
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo", "Salvo com sucesso") ; 
            context.addMessage("dasdasasd", message);
           
        }catch(Exception ex){
            ///deu erro 
        }
        
        
        
    }
    
    public void novo(){
        this.autor = new Autor();
    }
    
    
    public List<Autor> getLista(){
        return dao.findAll();
    }
   
    
    
    
}

